<?php

namespace JyMeituan;

/**
 * Class Factory
 *
 * @method static \JyMeituan\Init\Application               Init(array $config)
 */
class Meituan
{
  /**
   * @param string $name
   * @param array  $config
   *
   * @return ServiceContainer
   */
  public static function make($name, $config = [])
  {
    $namespace   = self::studly($name);
    $application = "\\JyMeituan\\{$namespace}\\Application";
    return new $application($config);
  }
  
  /**
   * Dynamically pass methods to the application.
   *
   * @param string $name
   * @param array  $arguments
   *
   * @return mixed
   */
  public static function __callStatic($name, $arguments)
  {
    return self::make($name, ...$arguments);
  }
  
  public static function studly($value)
  {
    $value = ucwords(str_replace(['-', '_'], ' ', $value));
    return str_replace(' ', '', $value);
  }
}
