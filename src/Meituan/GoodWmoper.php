<?php

namespace JyMeituan\Meituan;

/**
 * 外卖非接单，菜单API
 */
trait GoodWmoper
{
    /**
     * 查询门店菜品分类列表
     * https://developer.meituan.com/docs/api/wmoper-ng-food-queryFoodCatList
     *
     * @return false|mixed
     */
    public function getFoodCatList()
    {
        return $this->request('/wmoper/ng/food/queryFoodCatList', ['biz' => json_encode([])]);
    }
    
    /**
     * 查询门店菜品列表
     * https://developer.meituan.com/docs/api/wmoper-ng-food-queryFoodList
     *
     * @param int $page  页码
     * @param int $limit 每页返回的数量
     * @return void
     */
    public function getFoodList($page = 1, $limit = 20)
    {
        $param = [
            'offset' => ($page - 1) * $limit,
            'limit'  => $limit ?: 20,
        ];
        return $this->request('/wmoper/ng/food/queryFoodList', ['biz' => json_encode($param)]);
    }
    
    /**
     * 获取菜品属性
     * https://developer.meituan.com/docs/api/wmoper-ng-food-queryFoodPropertyList
     *
     * @param int $app_food_code 菜品id
     * @return false|mixed
     */
    public function getFoodPropertyList($app_food_code)
    {
        $param = ['app_food_code' => $app_food_code];
        return $this->request('/wmoper/ng/food/queryFoodPropertyList', ['biz' => json_encode($param)]);
    }
    
    /**
     * 查询菜品详情
     * https://developer.meituan.com/docs/api/wmoper-ng-food-detail
     *
     * @param int $app_food_code 菜品id
     * @return false|mixed
     */
    public function getFoodDetail($app_food_code)
    {
        $param = ['app_food_code' => $app_food_code];
        return $this->request('/wmoper/ng/food/detail', ['biz' => json_encode($param)]);
    }
}
