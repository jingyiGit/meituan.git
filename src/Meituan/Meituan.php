<?php

namespace JyMeituan\Meituan;

use JyMeituan\Kernel\Http;

class Meituan
{
    use Order;
    use ShopWmoper;
    use GoodWmoper;
    use OrderWmoper;
    
    protected $domainUrl = 'https://api-open-cater.meituan.com';
    protected $config = [];
    protected $appAuthToken = '';
    protected $error = null;
    
    public function __construct($config = null)
    {
        $this->config = $config;
    }
    
    /**
     * 创建授权链接，暂时没用
     * 第三方业务授权 https://developer.meituan.com/docs/biz/comm-dev-isv-auth
     */
    public function auth($state = '')
    {
        $data         = [
            'developerId' => $this->config['developerId'],
            'businessId'  => 31,
            'timestamp'   => time(),
            'charset'     => 'UTF-8',
            'state'       => $state,
        ];
        $data['sign'] = $this->getSign($data);
        return 'https://open-erp.meituan.com/general/auth?' . http_build_query($data);
    }
    
    /**
     * UISDK接入
     * https://developer.meituan.com/docs/biz/biz_waimaing_df60ceb0-f17d-403e-8a13-884b2ee14555
     *
     * @param string $ePoiId     ERP厂商针对该门店自行定义的id
     * @param string $ePoiName   ERP商家门店名
     * @param string $businessId 业务类型 1团购、2外卖、3闪惠、5支付、7预定、8全渠道会员、16外卖(非接单)
     * @return string
     */
    public function auth_UISDK($ePoiId = '', $ePoiName = '', $businessId = 2)
    {
        $data         = [
            'developerId' => $this->config['developerId'],
            'businessId'  => $businessId,
            'timestamp'   => time(),
            'ePoiId'      => $ePoiId,
            'ePoiName'    => $ePoiName,
        ];
        $data['sign'] = $this->getSign($data);
        return 'https://open-erp.meituan.com/storemap?' . http_build_query($data);
    }
    
    /**
     * UISDK解绑
     * https://developer.meituan.com/docs/biz/biz_waimaing_d3a3e69b-7ef8-470c-bc7c-b6b6c66441c5
     *
     * @param string $appAuthToken 认领门店返回的token【一店一token】
     * @param string $businessId   业务类型 1团购、2外卖、3闪惠、5支付、7预定、8全渠道会员、16外卖(非接单)
     * @return string
     */
    public function auth_UISDK_un($appAuthToken, $businessId = 2)
    {
        $data         = [
            'businessId'   => $businessId,
            'timestamp'    => time(),
            'appAuthToken' => $appAuthToken,
        ];
        $data['sign'] = $this->getSign($data);
        return 'https://open-erp.meituan.com/releasebinding?' . http_build_query($data);
    }
    
    public function setShopAppAuthToken($appAuthToken)
    {
        $this->appAuthToken = $appAuthToken;
    }
    
    /**
     * 设置开发者绑定门店的部分消息停止推送
     * https://developer.meituan.com/docs/api/common-developer-setBlockMsgList
     *
     * @param string $targetBusinessId       接口操作的授权资源对应的业务id，1团购、2外卖、3闪惠、5支付、7预订、8全渠道会员、16外卖非接单
     * @param array  $blockMsgList           需要停止推送的消息类型列表，清空停止推送的消息列表，该参数传空列表或者不传值即可
     *                                       https://developer.meituan.com/docs/biz/biz_xn202005112_5ae7f9f0-a637-4c77-99de-4bb2a4d57f60
     * @return false|mixed
     */
    public function stopCallPush($targetBusinessId, $blockMsgList = [])
    {
        $params = [
            'targetAppAuthToken' => $this->appAuthToken,
            'targetBusinessId'   => $targetBusinessId,
            'blockMsgList'       => $blockMsgList,
        ];
        return $this->request('/common/developer/setBlockMsgList', ['biz' => json_encode($params)]);
    }
    
    /**
     * 查询开发者绑定门店停止推送的消息列表
     * https://developer.meituan.com/docs/api/common-developer-queryBlockMsgList
     *
     * @param string $targetBusinessId 接口操作的授权资源对应的业务id，1团购、2外卖、3闪惠、5支付、7预订、8全渠道会员、16外卖非接单)
     * @return false|mixed
     */
    public function getStopCallPushList($targetBusinessId)
    {
        $params = [
            'targetAppAuthToken' => $this->appAuthToken,
            'targetBusinessId'   => $targetBusinessId,
        ];
        return $this->request('/common/developer/queryBlockMsgList', ['biz' => json_encode($params)]);
    }
    
    /**
     * 公共请求
     *
     * @remark 公共响应参数 https://developer.meituan.com/docs/biz/biz-comm-sys-param-resp
     * @remark 平台返回状态码 https://developer.meituan.com/docs/biz/comm-errcode1
     * @param string $path   请求path，如：/wmoper/order/queryOrderDetail
     * @param array  $params 请求参数
     * @return false|mixed
     */
    private function request($path, $params)
    {
        $params         = $this->handleGlobalParam($params);
        $params['sign'] = $this->getSign($params);
        $headers        = [
            'Content-Type' => 'application/x-www-form-urlencoded;charset=utf-8',
        ];
        if (strpos($path, 'http') === false) {
            $url = $this->domainUrl . $path;
        } else {
            $url = $path;
        }
        $res = Http::httpPostRaw($url, http_build_query($params), $headers);
        $res = json_decode($res, true);
        if (isset($res['code']) && $res['code'] == 'OP_SUCCESS') {
            return $res;
        }
        return $this->setError($res);
    }
    
    /**
     * 获取签名
     *
     * @param array $data
     * @return string
     */
    private function getSign($data)
    {
        if ($data == null) {
            return null;
        }
        ksort($data);
        $result_str = "";
        foreach ($data as $key => $val) {
            if ($key != null && $key != "" && $key != "sign") {
                $result_str = $result_str . $key . $val;
            }
        }
        $result_str = $this->config['SignKey'] . $result_str;
        return bin2hex(sha1($result_str, true));
    }
    
    /**
     * 处理全局参数并签名
     *
     * @param $param
     * @return array
     */
    private function handleGlobalParam($param)
    {
        // 全局参数
        $params         = array_merge([
            'appAuthToken' => $this->appAuthToken,
            'charset'      => 'UTF-8',
            'timestamp'    => time(),
            'version'      => 2,
            'developerId'  => (int)$this->config['developerId'],
            'businessId'   => $param['businessId'] ?: 2,
        ], $param);
        $params['sign'] = $this->getSign($params);
        return $params;
    }
    
    private function handleReturn($res)
    {
        if ($res['code'] == 0 && $res['apiMessage'] == null) {
            $this->error = null;
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    private function setError($error)
    {
        $this->error = $error;
        return false;
    }
}
