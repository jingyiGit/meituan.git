<?php

namespace JyMeituan\Meituan;

/**
 * 外卖非接单，订单API
 */
trait OrderWmoper
{
    /**
     * 查询订单列表
     * https://developer.meituan.com/docs/api/wmoper-ng-order-queryOrders
     *
     * @param $param
     * @return void
     */
    public function getOrderList($param = [])
    {
        if ($param['dayInt']) {
            $param['dayInt'] = strlen($param['dayInt']) == 10 ? (int)date('Ymd', $param['dayInt']) : (int)date('Ymd', time());
        } else {
            $param['dayInt'] = (int)date('Ymd', time());
        }
        $param['orderStatus'] = $param['orderStatus'] ?: -1;
        $param['pageNo']      = $param['pageNo'] ?: 1;
        $param['pageSize']    = $param['pageSize'] ?: 10;
        
        // 开始请求
        return $this->request('/wmoper/ng/order/queryOrders', ['businessId' => 16, 'biz' => json_encode($param)]);
    }
    
    /**
     * 查询订单详情
     * https://developer.meituan.com/docs/api/wmoper-order-queryOrderDetail
     *
     * @return void
     */
    public function queryOrderDetail($orderId)
    {
        $param = ['orderId' => $orderId];
        return $this->request('/wmoper/order/queryOrderDetail', ['biz' => json_encode($param)]);
    }
    
    /**
     * 查询订单详情(展示费率相关字段)
     * https://developer.meituan.com/docs/api/wmoper-ng-order-queryDetail
     *
     * @param int $orderId 订单ID
     * @return void
     */
    public function queryDetail($orderId)
    {
        $param = [
            'orderId' => $orderId,
        ];
        return $this->request('/wmoper/ng/order/queryDetail', ['biz' => json_encode($param)]);
    }
    
    /**
     * 自配订单同步配送信息
     * https://developer.meituan.com/docs/api/wmoper-ng-order-riderPosition
     *
     * @return void
     */
    public function riderPosition($param)
    {
        return $this->request('/wmoper/ng/order/riderPosition', ['biz' => json_encode($param)]);
    }
    
    /**
     * 查询自配送订单的收货人信息
     * https://developer.meituan.com/docs/api/wmoper-ng-delivery-getRecipientInfo
     *
     * @return void
     */
    public function getRecipientInfo($orderId)
    {
        $param = ['orderId' => $orderId];
        return $this->request('/wmoper/ng/delivery/getRecipientInfo', ['biz' => json_encode($param)]);
    }
    
    /**
     * 查询门店二维码
     * https://developer.meituan.com/docs/api/wmoper-ng-poi-getPoiExtendInfo
     *
     * @return void
     */
    public function getPoiExtendInfo()
    {
        return $this->request('/wmoper/ng/poi/getPoiExtendInfo', []);
    }
}
