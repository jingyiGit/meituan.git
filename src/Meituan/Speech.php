<?php
// 语音合成

namespace JyMeituan\Meituan;

use JyUtils\Str\Str;

trait Speech
{
  /**
   * 流式语音合成接口
   * https://developer.meituan.com/docs/api/speech-tts-v1-stream
   *
   * @param array $param
   */
  public function speechStream($param)
  {
    $param['session_id']   = Str::uuid('');
    $param['voice_name']   = $param['voice_name'] ?: 'meifannan';
    $param['audio_format'] = $param['audio_format'] ?: 'pcm';
    $param['speed']        = $param['speed'] ?: 50;
    $param['volume']       = $param['volume'] ?: 50;
    $param['agc']          = $param['agc'] ?: 1;
    $param['sample_rate']  = $param['sample_rate'] ?: 16000;
    // 开始请求
    $res = $this->request('/speech/tts/v1/stream', ['biz' => json_encode($param)]);
    
    dd('返回' . $res);
  }
  
  /**
   * 整句语音合成接口
   * https://developer.meituan.com/docs/api/speech-tts-v1-file
   *
   * @param array $param
   */
  public function fileStream($param)
  {
    $param['session_id']  = Str::uuid('');
    $param['speed']       = $param['speed'] ?: 50;
    $param['volume']      = $param['volume'] ?: 50;
    $param['agc']         = $param['agc'] ?: 1;
    $param['sample_rate'] = $param['sample_rate'] ?: 16000;
    // 开始请求
    $res = $this->request('/speech/tts/v1/file', ['biz' => json_encode($param)]);
    
    dd('返回' . $res);
  }
}
